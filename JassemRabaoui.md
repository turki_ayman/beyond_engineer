8 novembre : réalisation de la page welcome (HTML+CSS):  
- collection des données(photos , les couleurs qu'on va adapter ...)
- réalisation de la page html:(welcome.html)
		création du navbar 
		création des inputs pour l'email et le password 
		créations des deux buttons pour le Login ou bien la création d'un compte sans oublié les liens au cas où quelqu'un oublie son compte  
- faire le style de la page à l'aide de css (welcome.css)
9 novembre : réalisation de la page Inscription (HTML+CSS) : 
- collection des données(photos , les couleurs qu'on va adapter ...)
- réalisation de la page html :(inscription.html) 
		création du navbar
		création des inputs pour l'email et le password
		création du boutton de l'inscription
- faire le style de la page à l'aide de css: (inscription.css)
13-14 novembre : réalisation de la page Forum (HTML+CSS)
- collection des données(photos , les couleurs qu'on va adapter ...)
- réalisation de la page html (forum.html)
		création du navbar
		création des cartes qui vont donner les choix existants ainsi que quelques informations sur le forum (la place , la date ...)
		création des buttons dans les cartes qui vont servir d'aller au grande page qui contient la date , le prix , une petite description ainsi que le teaser de l'évenement.
- faire le style de la page à l'aide de css(forum.css)
		faire un style attirant pour l'utilisateur en employant le hover pour le navbar et le shadow pour les cartes.
17-18 novembre : réalisation de la page forum sup'com (HTML+CSS)
- collection des données(photos , les couleurs qu'on va adapter ...)
- réalisation de la page html (forum_supcom.html)
		création d'une page qui va contenir toutes les informations du forum sup'com en employant la date,le prix, la place ou va se dérouler l'évenement en incluant l'affiche du forum et leur teaser et j'ai utiliser un boutton qui sert à guider l'utilisateur pour s'inscrire en ligne en remplissant un formulaire.
		j'ai divisé la page en deux colonnes : une contient l'affiche et le teaser ainsi que la description de l'évenement et autre pour s'inscrire au évenement en cliquant sur un boutton
- faire le style de la page à l'aide de css(forum_supcom.css)
20-22 novembre : réalisation des deux pages forum insat/ensit (HTML+CSS)
- collection des données(photos , les couleurs qu'on va adapter ...)
- réalisation de la page html (forum_insat.html/forum_ensit.html)
- faire le style de la page à l'aide de css(forum_insat.css/forum_ensit.css)

24-26 novembre : réalisation de la page NRW (html+css)
- collection des données(photos , les couleurs qu'on va adapter ...)
- réalisation de la page html (nrw.html)
- faire le style de la page à l'aide de css(nrw.css)