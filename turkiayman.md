8-16 novembre: développement de la page d'accueil (Html , Css et Javascript) : 
            *** page d'accueil.html + cssacueil.css ***

             - division de la page html pour construire la squelette ( structure globale ) 
			 - création de la barre de navigation avec notre logo( crée par canvas ) et les boutons de navigation 
			 - création de Slides Show avec l'animation de transition automatique 
			 - création 4 Cards qui contiennent les types des categories des évènements avec leurs boutons 
			 - création des 4 Flip Cards des évènements de cette semaine et 2 Cards avec animations pour modéliser les évènements à venir 
			 - création de pied de page (Footer) avec des icons animés
			 - collection des données ( images et descriptions des évènements) et les implémenter dans la page d'accueil 
			 - retouche Css pour bien placer les élements dans la page et pour la décoration ( les couleurs , background , shadows , ...) 
			 
17-24 novembre: développement des pages des fétes : 
            *** parties.html + parties.css *** *** karaoké.html + karaoke.css *** ***night party.html + night.css *** 
			               *** white party.html + white.css ***
             
			 - collection des données à propos les fétes ( images et descriptions)
			 - Template qui contient tous les fétes ( html et css ) : 3 Cards 
			 - Template pour karaoké party 
			 - Template pour night party
			 - Template pour white party
			 - faire le lien entre les pages html et les boutons "see more" dans la page d'accueil pour naviguer 

25-26 novembre : développement de la page de TSYP event :
               ***eventTSYP.html + eventTSYP.css***
			   
			   - collection des données à propos cet événement
			   -Template
			   -intégration des données dan la pages et faire le style avec css

27-28 novembre : faire fonctionner les boutons de la page d'accueil :
             
			 - créer les path et faire les liaisons des boutons de la page d'accueil avec les autres pages de mes collégues 
			   selon la fonctionnalité de chaque bouton 
			 